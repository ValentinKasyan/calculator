package calculator.com.kultprosvet.calculatorb;


import android.content.Context;
import android.widget.Toast;

import static android.app.PendingIntent.getActivity;

public class CalculatorBrain {
    // 3 + 6 = 9
    // 3 и 6 операнд
    //  + оператор
    // 9 результат
    private double mOperand;
    private double mWaitingOperand;
    private String mWaitingOperator;
    private double mCalculatorMemory;

    // типы операций
    public static final String ADD = "+";
    public static final String SUBTRACT = "-";
    public static final String MULTIPLY = "*";
    public static final String DIVIDE = "/";

    public static final String CLEAR = "C" ;
    public static final String CLEARMEMORY = "MC";
    public static final String ADDTOMEMORY = "M+";
    public static final String SUBTRACTFROMMEMORY = "M-";
    public static final String RECALLMEMORY = "MR";
   



    // constructor
    public CalculatorBrain() {
        // инициализируем значения для начала
        mOperand = 0;
        mWaitingOperand = 0;
        mWaitingOperator = "";
        mCalculatorMemory = 0;
    }

    public void setOperand(double operand) {
        mOperand = operand;
    }

    public double getResult() {
        return mOperand;
    }
    // TODO: 29.06.2016   реализовать поворот и применить setMemory in class CalculatorBrain
    // для изменения ориентации экрана
    /*
    public void setMemory(double calculatorMemory) {
        mCalculatorMemory = calculatorMemory;
    }


    // для изменения ориентации экрана
    public double getMemory() {
        return mCalculatorMemory;
    }
    */
    public String toString() {
        return Double.toString(mOperand);
    }

    protected double performOperation(String operator) {


        if (operator.equals(CLEAR)) {
            mOperand = 0;
            mWaitingOperator = "";
            mWaitingOperand = 0;
            // mCalculatorMemory = 0;
            //операции с памятью
        } else if (operator.equals(CLEARMEMORY)) {
            mCalculatorMemory = 0;
        } else if (operator.equals(ADDTOMEMORY)) {
            mCalculatorMemory = mCalculatorMemory + mOperand;
        } else if (operator.equals(SUBTRACTFROMMEMORY)) {
            mCalculatorMemory = mCalculatorMemory - mOperand;
        } else if (operator.equals(RECALLMEMORY)) {
            mOperand = mCalculatorMemory;
        }   else {
            //остальные операции
            performWaitingOperation();
            mWaitingOperator = operator;
            mWaitingOperand = mOperand;
        }

        return mOperand;
    }

    protected void performWaitingOperation() {

        if (mWaitingOperator.equals(ADD)) {
            mOperand = mWaitingOperand + mOperand;
        } else if (mWaitingOperator.equals(SUBTRACT)) {
            mOperand = mWaitingOperand - mOperand;
        } else if (mWaitingOperator.equals(MULTIPLY)) {
            mOperand = mWaitingOperand * mOperand;
        } else if (mWaitingOperator.equals(DIVIDE)) {
            if (mOperand != 0) {
                mOperand = mWaitingOperand / mOperand;
            }else{

            }

        }

    }


    
}
