package calculator.com.kultprosvet.calculatorb;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.N)//для использования DecimalFormat
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView mCalculatorDisplay;
    private Boolean userIsInTheMiddleOfTypingANumber = false;
    private CalculatorBrain mCalculatorBrain;
    private static final String DIGITS = "0123456789.";

    //выводит количество знаков в десятичном формате
    DecimalFormat df = new DecimalFormat("@###########");


    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCalculatorBrain = new CalculatorBrain();
        mCalculatorDisplay = (TextView) findViewById(R.id.textView1);

        df.setMinimumFractionDigits(0);//Устанавливает минимальное число цифр, разрешенных в дробной части числа (после нажатия " = ") .
        df.setMinimumIntegerDigits(1);//Устанавливает минимальное число цифр, разрешенных в целой части числа (после нажатия " = ") .
        df.setMaximumIntegerDigits(8);//Устанавливает максимальное число цифр, разрешенных в целой части числа (после нажатия " = ") .

        findViewById(R.id.button0).setOnClickListener(this);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
        findViewById(R.id.button5).setOnClickListener(this);
        findViewById(R.id.button6).setOnClickListener(this);
        findViewById(R.id.button7).setOnClickListener(this);
        findViewById(R.id.button8).setOnClickListener(this);
        findViewById(R.id.button9).setOnClickListener(this);

        findViewById(R.id.buttonAdd).setOnClickListener(this);
        findViewById(R.id.buttonSubtract).setOnClickListener(this);
        findViewById(R.id.buttonMultiply).setOnClickListener(this);
        findViewById(R.id.buttonDivide).setOnClickListener(this);
        findViewById(R.id.buttonToggleSign).setOnClickListener(this);
        findViewById(R.id.buttonDecimalPoint).setOnClickListener(this);
        findViewById(R.id.buttonEquals).setOnClickListener(this);
        findViewById(R.id.buttonClear).setOnClickListener(this);
        findViewById(R.id.buttonClearMemory).setOnClickListener(this);
        findViewById(R.id.buttonAddToMemory).setOnClickListener(this);
        findViewById(R.id.buttonSubtractFromMemory).setOnClickListener(this);
        findViewById(R.id.buttonRecallMemory).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //определяем нажатую кнопку чепез считывание значение последней и перевода в String
        String buttonPressed = ((Button) v).getText().toString();

        if (DIGITS.contains(buttonPressed)) {

            // число из приведенных в String DIGITS нажато
            if (userIsInTheMiddleOfTypingANumber) {

                if (buttonPressed.equals(".") && mCalculatorDisplay.getText().toString().contains(".")) {
                    // ERROR PREVENTION
                    // не допускаем ввод нескольких знаков после запятой
                } else {
                    mCalculatorDisplay.append(buttonPressed); //вывод нажатой кнопки (обычный вывод)
                }

            } else {

                if (buttonPressed.equals(".")) {
                    // ERROR PREVENTION
                    //когда нажата кнопка "." то перед ней ставим 0
                    mCalculatorDisplay.setText(0 + buttonPressed);
                } else {
                    mCalculatorDisplay.setText(buttonPressed);//вывод нажатой кнопки (обычный вывод)

                }

                userIsInTheMiddleOfTypingANumber = true;
            }

        } else {
            // число не нажато, а нажата операция
            if (userIsInTheMiddleOfTypingANumber) {

                mCalculatorBrain.setOperand(Double.parseDouble(mCalculatorDisplay.getText().toString()));
                userIsInTheMiddleOfTypingANumber = false;
            }

            mCalculatorBrain.performOperation(buttonPressed);
            mCalculatorDisplay.setText(df.format(mCalculatorBrain.getResult()));

        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // TODO: 29.06.2016 реализовать поворот и применить onSaveInstanceState  
        //сохраняем изменения на экране при смене ориентаций(не реализовал)
        outState.putDouble("OPERAND", mCalculatorBrain.getResult());
       // outState.putDouble("MEMORY", mCalculatorBrain.getMemory());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // TODO: 29.06.2016 реализовать поворот и применить onRestoreInstanceState
        // Восстановить изменения на экране при смене ориентаций (не реализовал)
        mCalculatorBrain.setOperand(savedInstanceState.getDouble("OPERAND"));
        // TODO: 29.06.2016 реализовать поворот и после работы метода setMemory доделать
       // mCalculatorBrain.setMemory(savedInstanceState.getDouble("MEMORY"));
       // mCalculatorDisplay.setText(df.format(mCalculatorBrain.getResult()));

    }


}
